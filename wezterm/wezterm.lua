local wezterm = require 'wezterm'
local config = {}
----------------
--> Settings <--
----------------
config.font = wezterm.font_with_fallback({ { family = 'Lilex Nerd Font Mono' }, })
config.font_size = 13.3
config.color_scheme = 'Snazzy (base16)'
config.adjust_window_size_when_changing_font_size = false
config.animation_fps = 60
config.cursor_blink_ease_in = 'Linear'
config.cursor_blink_ease_out = 'Linear'
config.cursor_blink_rate = 1100
config.default_cursor_style = 'BlinkingUnderline'
config.cursor_thickness = '0.05cell'
config.automatically_reload_config = true
config.hide_tab_bar_if_only_one_tab = true
config.enable_tab_bar = true
config.audible_bell = 'Disabled'
config.window_close_confirmation = 'NeverPrompt'
config.initial_cols = 80
config.initial_rows = 24
config.integrated_title_button_color = "Auto"
config.integrated_title_button_style = "Gnome"
config.integrated_title_buttons = { 'Close' }
config.line_height = 1.0
config.cell_width = 1.0
config.min_scroll_bar_height = "0.5cell"
config.mouse_wheel_scrolls_tabs = false
config.pane_focus_follows_mouse = true
config.prefer_to_spawn_tabs = false
config.scrollback_lines = 3500
config.use_fancy_tab_bar = false
config.show_tabs_in_tab_bar = true
config.show_new_tab_button_in_tab_bar = true
config.show_tab_index_in_tab_bar = true
config.show_tabs_in_tab_bar = true
config.status_update_interval = 1000
config.swallow_mouse_click_on_pane_focus = true
config.swallow_mouse_click_on_window_focus = true
config.switch_to_last_active_tab_when_closing_tab = true
config.use_resize_increments = true
config.window_padding = {
    left = 2,
    right = 2,
    top = 0,
    bottom = 0,
  }
config.foreground_text_hsb = {
    hue = 1.0,
    saturation = 1.5,
    brightness = 1.0,
  }
return config
